#include "interrupt.h"
#include "DAVE.h"
#include "water_pump.h"

#define ONEMILISEC 1000U


T_U8	interrupt_reg = 0;
void INTERRUPT_Check();

void interrupt_init()
{
	interrupt_reg = 0;

	uint32_t TimerId;
	SYSTIMER_STATUS_t status;
	// Create Software timer
	TimerId = (uint32_t)SYSTIMER_CreateTimer(ONEMILISEC,SYSTIMER_MODE_PERIODIC,(void*)INTERRUPT_Check,NULL);

	if (TimerId != 0U)

	{

	//timer is created successfully, now start/run software timer

	status = SYSTIMER_StartTimer(TimerId);

	if (status == SYSTIMER_STATUS_SUCCESS)

	{

	// Software timer is running
	// Add user code here

	}

	else

	{

	// Error during software timer start operation

	}

	}

	else

	{

	// timer ID Can not be zero

	}
}

void _activate_isr(interrupts isr)
{
	interrupt_reg |= 1 <<isr;

}

void _deactive_isr(interrupts isr)
{
	interrupt_reg &= 0<< isr;
}




void INTERRUPT_Check()
{

	if( interrupt_reg & (1 << START_PUMP_int ))
		{
		start_waterPump();
		_deactive_isr(START_PUMP_int );
		}

	if( interrupt_reg & (1 << STOP_PUMP_int ))
		{
		stop_waterPump();
		_deactive_isr(START_PUMP_int );
		_deactive_isr(STOP_PUMP_int );
		}


}

