
 /********************************************************************************
* COPYRIGHT (C) CONTINENTAL AG 2017
* ALL RIGHTS RESERVED.
*
* The reproduction, transmission or use of this document or its
* contents is not permitted without express written authority.
* Offenders will be liable for damages. All rights, including rights
* created by patent grant or registration of a utility model or design,
* are reserved.
*********************************************************************************
*
*  File name:           $Source: task.h $
*  Created on:			$Oct 20, 2016 $
*  Author:              uidn3647
*  Module acronym:      $OS $
*  Originator:          ...
*  Specification:       ...
*  Date:                $Date: 2017/10/12 10:09:26CEST $
*
*  Description:			OS module normal header for signalling
*
*********************************************************************************
*
*  Changes:
*  $Log: task.h  $
*
*  Initial revision
*  Formatting done according to standard template
*
*********************************************************************************/
#ifndef OS_TASK_H_
#define OS_TASK_H_ 1

/***********************************************************************************************************************
*  include files
***********************************************************************************************************************/

/**********************************************************************************************************************
*  global symbols and macros
**********************************************************************************************************************/


/**********************************************************************************************************************
*  global constants (CONST) and enumerations (ENUM)
**********************************************************************************************************************/


/**********************************************************************************************************************
*  global type definitions (STRUCT, TYPEDEF, ...)
**********************************************************************************************************************/


/**********************************************************************************************************************
*  global variable declarations (exported variables EXTERN)
**********************************************************************************************************************/


/**********************************************************************************************************************
*  global function declarations (prototypes of exported functions EXTERN)
**********************************************************************************************************************/

void Task_v1ms(void);
void Task_v5ms(void);
void Task_v10ms(void);
void Task_v50ms(void);
void Task_v100ms(void);
void Task_v500ms(void);
void Task_v1000ms(void);
void Task_vBackground(void);
void Task_vInit (void);
#endif /* RTE_TASK_H_ */
/***********************************************************************************************************************
*  end of file
***********************************************************************************************************************/

