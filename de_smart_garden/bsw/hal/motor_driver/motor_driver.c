#include "motor_driver.h"

void enable_motor_a(){
	DIGITAL_IO_SetOutputHigh(&motor_driver_enable);
}

void disable_motor_a(){
	DIGITAL_IO_SetOutputLow(&motor_driver_enable);
}
