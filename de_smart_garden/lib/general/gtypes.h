#ifndef LIB_COMPILER_GTYPES_H
#define LIB_COMPILER_GTYPES_H 1

#include "DAVE.h"
#define NULL_PTR        ((void *)0)

#define TRUE                     (1==1)
#define FALSE                    (!TRUE)

typedef signed char T_S8; /*		 				-128 .. +127				*/
typedef signed short T_S16; /*	   				  -32768 .. +32767				*/
typedef signed long T_S32; /* 		 		 -2147483648 .. +2147483647			*/
typedef signed long long T_S64; /*	-9223372036854775808 .. +9223372036854775807*/
typedef unsigned char T_U8; /*	   					   0 .. 255					*/
typedef unsigned short T_U16; /* 					   0 .. 65535				*/
typedef unsigned long T_U32; /*         			   0 .. 4294967295			*/
typedef unsigned long long T_U64; /*				   0 .. 18446744073709551615*/
typedef unsigned char T_BOOL; /*  					TRUE/FALSE 					*/
typedef float T_F32;
typedef double T_F64;

#endif
