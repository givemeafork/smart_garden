#ifndef WATER_PUMP_H
#define WATER_PUMP_H 1

#include "gtypes.h"

typedef enum{
	pump_is_on,
	pump_is_off
}pump_state;

// these should be called when the server decides to!
T_U8 start_waterPump();
T_U8 stop_waterPump();
pump_state get_pump_status();

#endif
