#include "water_pump.h"
#include "motor_driver.h"

T_U8 pump_status = pump_is_off;


pump_state get_pump_status()
{
	return pump_status;
}

T_U8 start_waterPump()
{
	T_U8 ret_val = FALSE;
	pump_status = pump_is_on;

	// check if the pump is not already started
	if(!PWM_GetTimerStatus(&PWM_0))
   {
		enable_motor_a();
	 PWM_Start(&PWM_0);
	 ret_val = TRUE;
   }
	return ret_val;
}

T_U8 stop_waterPump()
{
	pump_status = pump_is_off;
	T_U8 ret_val = FALSE;
	disable_motor_a();
	// check if the pump is not already stopped
	if(PWM_GetTimerStatus(&PWM_0))
   {

	 PWM_Stop(&PWM_0);
	 ret_val = TRUE;
   }
	return ret_val;

}
