#include "bluetooth.h"
#include "rte.h"
enum headers {
	STATUS_GARDEN_REQUEST = 0x00,
	STATUS_GARDEN_RESPONSE = 0x01,

	START_PUMP = 0x02,
	HUMIDITY_IN_TARGET = 0x03,
	NO_WATER_ERROR = 0x04,
	HUMIDITY_SENSOR_ERROR = 0x05,

	XMC_STATE_REQ = 0x06,
	XMC_READY_STATE_ACK = 0x07,

	XMC_BUSY_STATE_ACK = 0x08,
	XMC_SIR_YES_SIR = 0x10,
	MSG_START_SIGN = 0x3C,
	MSG_END_SIGN = 0x3E
};

bluetooth_message received_message;

void bluetooth_transmit(bluetooth_message msg_to_send) {
	UART_Transmit(&UART_0, msg_to_send.buffer, sizeof(msg_to_send.buffer) - 1);
}

void rx_cb() {
	T_U8 index = 0;
	bluetooth_message transmit_message;

	while (index < 6 && (!XMC_USIC_CH_RXFIFO_IsEmpty(UART_0.channel))) {
		received_message.buffer[index] = UART_GetReceivedWord(&UART_0);

		index++;
	}

	if (received_message.part.msg_start == MSG_START_SIGN
			&& received_message.part.msg_end == MSG_END_SIGN) {
		transmit_message.part.msg_start = MSG_START_SIGN;
		transmit_message.part.msg_end = MSG_END_SIGN;
		switch (received_message.part.msg_type) {
		case STATUS_GARDEN_REQUEST: {
			transmit_message.part.msg_type = STATUS_GARDEN_RESPONSE;
			transmit_message.part.msg_content[humidity] = get_humidity_lvl();
			transmit_message.part.msg_content[temp] = 100;
			transmit_message.part.msg_content[water_lvl] = 100;
			bluetooth_transmit(transmit_message);
			break;
		}
		case START_PUMP: {
			activate_isr(START_PUMP_int);
			transmit_message.part.msg_type = XMC_SIR_YES_SIR;
			transmit_message.part.msg_content[humidity] = (~(0));
			transmit_message.part.msg_content[temp] = (~(0));
			transmit_message.part.msg_content[water_lvl] = (~(0));
			bluetooth_transmit(transmit_message);
			break;
		}
		case XMC_STATE_REQ: {
			transmit_message.part.msg_type = XMC_READY_STATE_ACK;
			transmit_message.part.msg_content[humidity] = (~(0));
			transmit_message.part.msg_content[temp] = (~(0));
			transmit_message.part.msg_content[water_lvl] = (~(0));
			bluetooth_transmit(transmit_message);
			break;
		}
		}
	} else {
		//TODO: send req to send again?
	}

}

void setup_bluetooth() {
	UART_SetRXFIFOTriggerLimit(&UART_0, 3);
}
