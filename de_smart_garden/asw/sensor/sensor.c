#include "sensor.h"


void add_to_buffer(XMC_VADC_RESULT_SIZE_t value)
{
	if (humidity_buffer_samples.index>=NR_OF_SAMPLES)
	{
		humidity_buffer_samples.index=0;
	}
	humidity_buffer_samples.buffer[humidity_buffer_samples.index]=value;
	humidity_buffer_samples.index++;
}

void Adc_Humidity_Handler()
{
	XMC_VADC_RESULT_SIZE_t result = 0;
	#if(UC_SERIES != XMC11)
    	result = ADC_MEASUREMENT_GetResult(&ADC_MEASUREMENT_Channel_A);
  #else
    result = ADC_MEASUREMENT_GetGlobalResult();
  #endif
   add_to_buffer(result);
}
