#ifndef SENSOR_H
#define SENSOR_H 1

#include "gtypes.h"


#define NR_OF_SAMPLES 10


typedef struct {
	unsigned char index;
	XMC_VADC_RESULT_SIZE_t buffer[NR_OF_SAMPLES];
}sensor_samples;

sensor_samples humidity_buffer_samples = {0, {0}};
void Adc_Humidity_Handler();

#endif
