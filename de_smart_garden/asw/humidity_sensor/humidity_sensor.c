#include "humidity_sensor.h"
#include "rte.h"
sensor_samples humidity_buffer_samples = {0, {0}};

#define flood	150
#define drought 2700

#define aprox_dif	100



void add_to_buffer(XMC_VADC_RESULT_SIZE_t value)
{
	if (humidity_buffer_samples.index>=NR_OF_SAMPLES)
	{
		humidity_buffer_samples.index=0;
	}
	humidity_buffer_samples.buffer[humidity_buffer_samples.index]=value;
	humidity_buffer_samples.index++;
}

void Adc_Humidity_Handler()
{
	XMC_VADC_RESULT_SIZE_t result = 0;
	#if(UC_SERIES != XMC11)
    	result = ADC_MEASUREMENT_GetResult(&ADC_MEASUREMENT_Channel_A);
  #else
    result = ADC_MEASUREMENT_GetGlobalResult();
  #endif
   add_to_buffer(result);
}

T_U8 humidity_level_old=0;
T_U8 humidity_level=0;
T_U8 calculate_humidity_level()
{
	T_U8 humidity_status;
	XMC_VADC_RESULT_SIZE_t humidity_level = 0;
	for(int i=0;i<NR_OF_SAMPLES;i++)
	{
		humidity_level += humidity_buffer_samples.buffer[i];
	}
	humidity_level /= NR_OF_SAMPLES;

	humidity_level -= flood;
	humidity_status = humidity_level * 100 / drought;
	humidity_status = 100 - humidity_status;

	humidity_level_old = humidity_level;
	humidity_level = humidity_status;

	if(humidity_level > humidity_level_old + aprox_dif)
	{
		if(get_pump_status() == pump_is_on)
		{
			activate_isr(STOP_PUMP_int);
		}
	}

	return humidity_status;
}



T_U8 get_humidity_level()
{
	return humidity_level;
}
