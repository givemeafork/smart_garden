#ifndef HUMIDITY_SENSOR_H
#define HUMIDITY_SENSOR_H 1

#include "gtypes.h"


#define NR_OF_SAMPLES 10


typedef struct {
	unsigned char index;
	XMC_VADC_RESULT_SIZE_t buffer[NR_OF_SAMPLES];
}sensor_samples;



void Adc_Humidity_Handler();
T_U8 get_humidity_level();
T_U8 calculate_humidity_level();

T_U8 get_garden_humidity_status();

#endif
