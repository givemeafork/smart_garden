
#ifndef RTE_RTE_HE_
#define RTE_RTE_HE_ 1



/**********************************************************************************************************************
 *  global symbols and macros
 **********************************************************************************************************************/
#include "DAVE.h"
#include "humidity_sensor.h"
#include "water_pump.h"
#include "interrupt.h"

/**********************************************************************************************************************
 *  global constants (CONST) and enumerations (ENUM EXTERN)
 **********************************************************************************************************************/

/**********************************************************************************************************************
 *  global type definitions (STRUCT, TYPEDEF, ... EXTERN)
 **********************************************************************************************************************/
#define pump_state pumpstate
/**********************************************************************************************************************
 *  global variable declarations (exported variables EXTERN)
 **********************************************************************************************************************/
#define get_humidity_lvl()	(get_humidity_level())
#define	calculate_humidity_level()	(calculate_humidity_level())
#define get_pump_status() (get_pump_status())
#define	start_water_pump()	(start_waterPump())
#define	stop_water_pump()	(stop_waterPump())
#define deactivate_isr(interrupts)	(_deactive_isr(interrupts))
#define activate_isr(interrupts)	(_activate_isr(interrupts))
/**********************************************************************************************************************
 *  global function declarations (prototypes of exported functions EXTERN)
 **********************************************************************************************************************/
void RTE_vInit(void);
void RTE_vTask1ms(void);
void RTE_vTask5ms(void);
void RTE_vTask10ms(void);
void RTE_vTask50ms(void);
void RTE_vTask100ms(void);
void RTE_vTask500ms(void);
void RTE_vTask1000ms(void);
void RTE_vBckTask(void);

#endif /* RTE_RTE_HE_ */
/***********************************************************************************************************************
 *  end of file
 ***********************************************************************************************************************/
